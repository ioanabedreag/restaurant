﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Exceptions
{
    class RestaurantException : ApplicationException
    {
        public RestaurantException(string message)
            : base(message)
        {
        }
    }
}