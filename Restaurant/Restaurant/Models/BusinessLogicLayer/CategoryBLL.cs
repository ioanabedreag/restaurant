﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Restaurant.Models.BusinessLogicLayer
{
    class CategoryBLL
    {
        public ObservableCollection<Category> CategoriyList { get; set; }



        CategoryDAL categoryDAL = new CategoryDAL();

        internal ObservableCollection<Category> GetAllActiveMenuCategories()
        {
            return categoryDAL.GetAllActiveCategories();
        }
    }
}
