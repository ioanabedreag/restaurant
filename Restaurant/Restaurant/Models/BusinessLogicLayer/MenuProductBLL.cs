﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.BusinessLogicLayer
{
    class MenuProductBLL
    {
        public ObservableCollection<MenuProduct> MenuProductList { get; set; }

        public ObservableCollection<Product> ProductList { get; set; }

        public MenuProductBLL()
        {
            MenuProductList = new ObservableCollection<MenuProduct>();
        }

        internal ObservableCollection<MenuProduct> GetMenuProductForCategory(Category menuCategory)
        {
            MenuProductList.Clear();
            MenuProductDAL menuProductDAL = new MenuProductDAL();
            var products = menuProductDAL.GetAllMenuProductsForCategory(menuCategory);
            foreach (var mp in products)
            {
                MenuProductList.Add(mp);
            }
            return products;
        }

        internal ObservableCollection<Product> GetProductsForMenuProduct(MenuProduct selectedMenuProduct)
        {
            MenuProductDAL menuProductDAL = new MenuProductDAL();
            return menuProductDAL.GetProductsForMenuProduct(selectedMenuProduct);
        }

    }
}
