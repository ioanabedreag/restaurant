﻿using Restaurant.Converters;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class CategoryDAL
    {
        internal ObservableCollection<Category> GetAllActiveCategories()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spCategorySelectAll", con);
                ObservableCollection<Category> result = new ObservableCollection<Category>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Category menuCategory = new Category();
                    menuCategory.CategoryId = (int)(reader[0]);
                    menuCategory.Name = reader.GetString(1);
                    menuCategory.Active = (bool)(reader[2]);
                    result.Add(menuCategory);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
