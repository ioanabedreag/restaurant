﻿using Restaurant.Models.EntityLayer;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class ClientDAL
    {
        internal void AddClient(Client client)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("spClientInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramFirstName = new SqlParameter("@FirstName", client.FirstName);
                SqlParameter paramLastName = new SqlParameter("@LastName", client.LastName);
                SqlParameter paramEmail = new SqlParameter("@Email", client.Email);
                SqlParameter paramPhone = new SqlParameter("@Phone", client.Phone);
                SqlParameter paramAddress = new SqlParameter("@Address", client.Address);
                SqlParameter paramPassword = new SqlParameter("@Password", client.Password);
                cmd.Parameters.Add(paramFirstName);
                cmd.Parameters.Add(paramLastName);
                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPhone);
                cmd.Parameters.Add(paramAddress);
                cmd.Parameters.Add(paramPassword);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal bool ClientExistByEmail(string email)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                bool result = false;
                SqlCommand cmd = new SqlCommand("spClientExistByEmail", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter paramEmail = new SqlParameter("@Email", email);
                cmd.Parameters.Add(paramEmail);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = Convert.ToBoolean(reader[0]);
                }
                reader.Close();
                return result;
            }
        }

        internal Client GetClientEmailPassword(string email, string password)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                Client client = new Client();
                SqlCommand cmd = new SqlCommand("spClientSelectEmailPassword", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter paramEmail = new SqlParameter("@Email", email);
                SqlParameter paramPassword = new SqlParameter("@Password", password);
                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPassword);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    client.ClientID = (int)reader[0];
                    client.FirstName = reader[1].ToString();
                    client.LastName = reader[2].ToString();
                    client.Email = reader[3].ToString();
                    client.Phone = reader[4].ToString();
                    client.Address = reader[5].ToString();
                    client.Password = reader[6].ToString();
                }
                reader.Close();
                return client;
            }
        }

    }
}

