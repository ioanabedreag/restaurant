﻿using Restaurant.Converters;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class MenuProductDAL
    {
        internal ObservableCollection<MenuProduct> GetAllMenuProductsForCategory(Category category)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                ObservableCollection<MenuProduct> result = new ObservableCollection<MenuProduct>();
                SqlCommand cmd = new SqlCommand("spGetProductFromCategory", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter categoryId = new SqlParameter("@CategoryId", category.CategoryId);
                cmd.Parameters.Add(categoryId);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new MenuProduct()
                    {
                        MenuProductId = reader.GetInt32(0),
                        CategoryId = reader.GetInt32(1),
                        Name = reader.GetString(2),
                        Description = reader.GetString(3),
                        Price = (decimal)(reader[4])
                    });
                }
                return result;
            }
        }

        internal ObservableCollection<Product> GetProductsForMenuProduct(MenuProduct product)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                ObservableCollection<Product> result = new ObservableCollection<Product>();
                SqlCommand cmd = new SqlCommand("spGetProductsForMenuProduct", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter MenuProductId = new SqlParameter("@MenuProductId", product.MenuProductId);
                cmd.Parameters.Add(MenuProductId);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new Product()
                    {
                        ProductId = (int)reader[0],
                        Name = (string)reader[1],
                        //Quantity = (int)reader[2]

                    });
                }
                return result;
            }
        }
    }
}
