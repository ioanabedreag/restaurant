﻿using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class OrderDAL
    {
        internal void AddOrder(Client client)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spOrderInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramClientId = new SqlParameter("@ClientId", (int)client.ClientID);
                cmd.Parameters.Add(paramClientId);
                con.Open();
                int reader = Convert.ToInt32(cmd.ExecuteScalar());
                int orderId = Convert.ToInt32(reader);


                foreach (CartMenuProduct product in client.CartProductsList)
                {
                    cmd = new SqlCommand("spOrderMenuProductInsert", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter paramOrderId = new SqlParameter("@OrderId", orderId);
                    SqlParameter paramMenuProductId = new SqlParameter("@MenuProductId", product.MenuProduct.MenuProductId);
                    cmd.Parameters.Add(paramOrderId);
                    cmd.Parameters.Add(paramMenuProductId);
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<Order> GetAllOrders()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spOrderSelectAll", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ObservableCollection<Order> result = new ObservableCollection<Order>();
                Random random = new Random();
                while (reader.Read())
                {
                    Order order = new Order()
                    {
                        OrderId = (int)reader[0],
                        Status = (string)reader[1],
                        ClientId = (int)reader[2],
                        OrderNumber = random.Next(0, 10000)

                    };
                    result.Add(order);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
