﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Category : BasePropertyChanged
    {
        private int? categoryId;

        public int? CategoryId
        {
            get
            {
                return categoryId;
            }
            set
            {
                categoryId = value;
                NotifyPropertyChanged("CategoryId");
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool active;

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
                NotifyPropertyChanged("Active");
            }
        }
    }
}
