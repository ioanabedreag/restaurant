﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Order : BasePropertyChanged
    {

        public int OrderId { get; set; }

        public string Status { get; set; }

        public int ClientId { get; set; }

        public int OrderNumber { get; set; }
    }
}
