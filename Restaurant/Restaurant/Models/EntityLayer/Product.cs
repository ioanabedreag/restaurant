﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Product : BasePropertyChanged
    {
        private int? productId;

        public int? ProductId
        {
            get
            {
                return productId;
            }
            set
            {
                productId = value;
                NotifyPropertyChanged("ProductId");
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private string stoc;

        public string Stoc
        {
            get
            {
                return stoc;
            }
            set
            {
                stoc = value;
                NotifyPropertyChanged("Stoc");
            }
        }

        private decimal price;
        public decimal Price
        {
            get
            {
                return price / 1.000000000000000000000000000000000m; ;
            }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }


        private int amount;

        public int Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
                NotifyPropertyChanged("Amount");
            }
        }

    }
}
