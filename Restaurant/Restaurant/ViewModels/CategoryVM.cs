﻿using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.ViewModels
{
    class CategoryVM : BasePropertyChanged
    {
        CategoryBLL categoryBLL = new CategoryBLL();
        public Category SelectedCategory { get; set; }
        public CategoryVM()
        {
            CategoryList = categoryBLL.GetAllActiveMenuCategories();
        }

        #region Data Members

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                NotifyPropertyChanged("ErrorMessage");
            }
        }

        public ObservableCollection<Category> CategoryList
        {
            get
            {
                return categoryBLL.CategoriyList;
            }
            set
            {
                categoryBLL.CategoriyList = value;
            }
        }

        #endregion
    }
}

