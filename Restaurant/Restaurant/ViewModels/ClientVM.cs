﻿using Restaurant.Exceptions;
using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels.Commands;
using Restaurant.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Restaurant.ViewModels
{
    class ClientVM : BasePropertyChanged
    {
        ClientDAL clientDAL = new ClientDAL();

        public ClientVM()
        {

        }

        #region Data Members

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                NotifyPropertyChanged("ErrorMessage");
            }
        }

        private string message;
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
                NotifyPropertyChanged("LastName");
            }
        }

        private string email;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
                NotifyPropertyChanged("Email");
            }
        }

        private string phone;
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
                NotifyPropertyChanged("Phone");
            }
        }

        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                NotifyPropertyChanged("Address");
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                NotifyPropertyChanged("Password");
            }
        }

        #endregion

        #region Command Members

        //button Register

        private ICommand registerCommand;
        public ICommand RegisterCommand
        {
            get
            {

                if (registerCommand == null)
                {
                    registerCommand = new RelayCommand<Client>(AddPerson);
                }
                return registerCommand;

            }
        }

        private ICommand loginUserCommand;
        public ICommand LoginUserCommand
        {
            get
            {

                if (loginUserCommand == null)
                {
                    loginUserCommand = new RelayCommand<Client>(LogInUser);
                }
                return loginUserCommand;

            }
        }


        #endregion

        #region Command Functions

        internal void AddPerson(object param)
        {
            try
            {
                if (String.IsNullOrEmpty(FirstName))
                {
                    throw new RestaurantException("FirstName lipsa!");
                }
                if (String.IsNullOrEmpty(LastName))
                {
                    throw new RestaurantException("LastName lipsa!");
                }
                if (String.IsNullOrEmpty(Email))
                {
                    throw new RestaurantException("Email lipsa!");
                }
                if (String.IsNullOrEmpty(Phone))
                {
                    throw new RestaurantException("Phone lipsa!");
                }
                if (!Phone.All(Char.IsDigit))
                {
                    throw new RestaurantException("Phone invalid!");
                }
                if (String.IsNullOrEmpty(Address))
                {
                    throw new RestaurantException("Adresa lipsa!");
                }
                if (String.IsNullOrEmpty(Password))
                {
                    throw new RestaurantException("Parola lipsa!");
                }
                if (clientDAL.ClientExistByEmail(email))
                {
                    throw new RestaurantException("Utilizatorul exista!");
                }
            }
            catch (RestaurantException re)
            {
                ErrorMessage = re.Message;
                return;
            }
            clientDAL.AddClient(new Client
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Phone = phone,
                Address = address,
                Password = password
            });

            FirstName = String.Empty;
            LastName = String.Empty;
            Email = String.Empty;
            Phone = String.Empty;
            Address = String.Empty;
            Password = String.Empty;
            Message = "Inregistrat!";
        }

        internal void LogInUser(object param)
        {
            if (String.IsNullOrEmpty(email))
            {
                ErrorMessage = "Insert valid Email";
                return;
            }
            if (String.IsNullOrEmpty(password))
            {
                ErrorMessage = "Insert valid Password";
                return;
            }
            Client loggedUser = clientDAL.GetClientEmailPassword(email, password);
            if (String.IsNullOrEmpty(loggedUser.FirstName))
            {
                ErrorMessage = "Incorrect Email or Password";
                return;
            }
            Application.Current.Resources["LoggedUser"] = loggedUser;
            Message = "Logat! Apasa butonul 'Back to Menu' pentru a te intoarce la meniu!" + ((Client)Application.Current.Resources["LoggedUser"]).FirstName + ((Client)Application.Current.Resources["LoggedUser"]).LastName;
        }

        #endregion
    }
}

