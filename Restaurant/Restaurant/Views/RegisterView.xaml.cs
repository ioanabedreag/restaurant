﻿using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant.Views
{
    /// <summary>
    /// Interaction logic for RegisterView.xaml
    /// </summary>
    public partial class RegisterView : UserControl
    {
        public RegisterView()
        {
            InitializeComponent();
        }

        public void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            Client loggedUser = (Client)Application.Current.Resources["LoggedUser"];
            if (loggedUser == null)
            {
                DataContext = new RegisterView();
            }
        }

        public void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            Client loggedUser = (Client)Application.Current.Resources["LoggedUser"];
            if (loggedUser == null)
            {
                DataContext = new LoginView();
            }
        }
    }
}
