﻿using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Restaurant.Views
{
    /// <summary>
    /// Interaction logic for WelcomeWiew.xaml
    /// </summary>
    public partial class WelcomeWiew : Window
    {
        public WelcomeWiew()
        {
            InitializeComponent();
            Application.Current.Resources["LoggedUser"] = null;
        }

        private void MenuBtn_Click(object sender, RoutedEventArgs e)
        {
            Window window = new MainWindow();
            window.Show();

            Close();
        }
    }
}
